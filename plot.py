import numpy as np
import matplotlib.pyplot as plt
from numpy import median
from scipy.stats.mstats import gmean
import statistics 

import re

plt.style.use('ggplot')

benchmarks = ['600.perlbench_s-210B','602.gcc_s-734B','605.mcf_s-665B','607.cactuBSSN_s-2421B',
				'619.lbm_s-4268B','620.omnetpp_s-874B','621.wrf_s-575B','623.xalancbmk_s-700B','625.x264_s-18B',
				'628.pop2_s-17B','631.deepsjeng_s-928B','638.imagick_s-10316B','641.leela_s-800B','644.nab_s-5853B','648.exchange2_s-1699B',
				'649.fotonik3d_s-1176B','654.roms_s-842B','657.xz_s-3167B','603.bwaves_s-3699B', '627.cam4_s-573B'] #'603.bwaves_s-3699B', ,'627.cam4_s-573B'

data_st = [0.144977, 1.039583, 0.729626, 0.240549, 0.069308, 0.083044, 0.205199, 0.14836, 0.3213, 0.338493, 0.378091, 0.835238, 0.151377, 3.56359, 0.234238, 0.142285, 0.438262, 1.249108, 0.287114, 0.172312]
prefetch_st = [5329.0, 59656.0, 1828.0, 54558.0, 338702.0, 188445.0, 69335.0, 358.0, 137587.0, 28747.0, 135074.0, 100711.0, 105331.0, 33200.0, 12337.0, 138.0, 55189.0, 48779.0, 3978.0, 25678.0]
#prefetchers = ['st_exa', 'bo_exa', 'nl_exa', 'am_exa', 'ip_exa', 'bo_exa_llc',''] #,'st_exa','bo_pre']
#prefetchers = ['st_exa','bo_exa_off54','bo_exa_rounds_100','bo_exa_else','bo_exa_list_52', 'bo_exa_list_52_opp','bo_exa_list_52_120','bo_exa_list_52_120_3']
#prefetchers = ['st_exa','bo_exa_off54','bo_exa_list_52_opp','bo_exa_list_52_200_thres_8_3','bo_exa_list_52_150_thres_8_else','bo_exa_list_52_190_thres_8_else','bo_exa_list_52_190_thres_8']
prefetchers = ['st_exa','am_exa','bo_exa_list_52_200_thres_8','bo_exa_l_52_r_200_t_8_bs_1_ls_15','bo_exa_l_52_r_200_t_8_bs_1_ls_20','bo_exa_l_52_r_200_t_8_bs_1_ls_20_llc','bo_exa_l_52_r_200_t_8_bs_1_ls_20_opp','bo_exa_l_52_r_200_t_8_bs_1_ls_25','bo_exa_l_52_r_200_t_8_bs_2_ls_20','bo_exa_l_52_r_200_t_8_bs_2_ls_20_llc','bo_exa_l_52_r_200_t_8_bs_3_ls_20','bo_exa_l_52_r_200_t_8_bs_3_ls_20_llc','bo_exa_l_52_r_200_t_8_bs_1_ls_20_if_dif','bo_exa_l_52_r_200_t_8_bs_1_ls_20_if_else'] 
prefetchers_all = ['st_exa','bo_exa_l_52_r_200_t_8_bs_1_ls_15','bo_exa_l_52_r_200_t_8_bs_1_ls_20','bo_exa_l_52_r_200_t_8_bs_1_ls_20_llc','bo_exa_l_52_r_200_t_8_bs_1_ls_20_opp','bo_exa_l_52_r_200_t_8_bs_1_ls_25','bo_exa_l_52_r_200_t_8_bs_2_ls_20','bo_exa_l_52_r_200_t_8_bs_2_ls_20_llc','bo_exa_l_52_r_200_t_8_bs_3_ls_20','bo_exa_l_52_r_200_t_8_bs_3_ls_20_llc','bo_exa_l_52_r_200_t_8_bs_1_ls_20_if_dif','bo_exa_l_52_r_200_t_8_bs_1_ls_20_if_else']
'''prefetchers_all = ['st_exa','bo_exa_off54','bo_exa_rounds_100','bo_exa_else','bo_exa_list_52', 'bo_exa_list_52_opp','bo_exa_list_52_95',
				   'bo_exa_list_52_90','bo_exa_list_52_115','bo_exa_list_52_120','bo_exa_list_52_120_3','bo_exa_list_52_120_4','bo_exa_list_52_120_5',
				   'bo_exa_list_52_100_thres_10','bo_exa_list_52_110_thres_10','bo_exa_list_52_120_thres_10','bo_exa_list_52_130_thres_10',
				   'bo_exa_list_52_100_thres_9','bo_exa_list_52_110_thres_9','bo_exa_list_52_120_thres_9','bo_exa_list_52_130_thres_9','bo_exa_list_52_140_thres_9','bo_exa_list_52_200_thres_9',
				   'bo_exa_list_52_160_thres_9','bo_exa_list_52_170_thres_9','bo_exa_list_52_180_thres_9','bo_exa_list_52_190_thres_9','bo_exa_list_52_200_thres_9',
				   'bo_exa_list_52_100_thres_8','bo_exa_list_52_150_thres_8','bo_exa_list_52_200_thres_8','bo_exa_list_52_190_thres_8',
				   'bo_exa_list_52_100_thres_8_else','bo_exa_list_52_150_thres_8_else','bo_exa_list_52_200_thres_8_else','bo_exa_list_52_200_thres_8_3']'''
extra = "" #-small_llc -low_bandwidth -scramble_loads
prefetch_nl = [1450377.0, 1030619.0, 534631.0, 1068416.0, 1881957.0, 1489038.0, 1593117.0, 1370156.0, 1008500.0, 1086661.0, 583251.0, 739575.0, 1531095.0, 515797.0, 1397370.0, 1637147.0, 1078886.0, 633357.0, 1159361.0, 1231763.0]
### GET THE IPC's 
def get_ipc(results_dir,benchmark,prefetcher):

	results_file = '{}/{}.champsimtrace.xz{}.{}'.format(results_dir,benchmark,extra,prefetcher)
	#print(results_file)

	with open(results_file) as f:
		for line in f:
			if re.findall('IPC:',line):
				#print(line)
				#print(re.findall('[0-9]+.[0-9]+',line)[2])
				return float(re.findall('[0-9]+.[0-9]+',line)[2])


def get_total_prefetch(results_dir,benchmark,prefetcher):
	results_file = '{}/{}.champsimtrace.xz{}.{}'.format(results_dir,benchmark,extra,prefetcher)
	#print(results_file)

	with open(results_file) as f:
		for line in f:
			if re.findall('prefetch:',line):
				#print(line)
				#print(re.findall('[0-9]+.[0-9]+',line)[2])
				return float(re.findall('[0-9]+',line)[0])



results_dir='/home/glopez/MIRI/Q3/APA/apa_dpc2/results'

def oldWay():
	#def main(argv):

	#results_dir='/home/glopez/MIRI/Q3/APA/apa_dpc2/local_results'
	results_dir='/home/glopez/MIRI/Q3/APA/apa_dpc2/results'
	data_nl = []
	data_st = []
	data_bo = []

	for b in benchmarks:
		for p in prefetchers:
			data_nl.append(0 if get_ipc(results_dir,b,prefetchers[0]) == None else get_ipc(results_dir,b,prefetchers[0]))
			data_st.append(0 if get_ipc(results_dir,b,prefetchers[1]) == None else get_ipc(results_dir,b,prefetchers[1]))
			data_bo.append(0 if get_ipc(results_dir,b,prefetchers[2]) == None else get_ipc(results_dir,b,prefetchers[2]))

	print(data_nl)
	print(data_st)
	print(data_bo)
	median_nl = median(data_nl)
	median_st = median(data_st)
	median_bo = median(data_bo)

	#data_nl/median_st
	#data_st/median_st'bo_exa_list_52_105'
	#data_bo/median_st
	#Normalize
	data_st_norm=[]
	data_nl_norm=[]
	data_bo_norm=[]

	for i in range(0,len(data_st)):
		#print(i)
		#print(data_st[i])
		#print(data_nl[i])
		#print(data_bo[i])
		#print(float(data_st[i]/data_st[i]))
		#print(float(data_nl[i]/data_st[i]))
		#print(float(data_bo[i]/data_st[i]))
		data_st_norm.append(float(data_st[i]/data_st[i]))
		data_nl_norm.append(float(data_nl[i]/data_st[i]))
		data_bo_norm.append(float(data_bo[i]/data_st[i]))

	N = len(benchmarks)+1
	#print(N)
	x = np.arange(N)
	xmin = 1
	#x = np.arange(xmin,N,2)
	print(x)

	data_nl_norm.append(median_nl)
	data_st_norm.append(median_st)
	data_bo_norm.append(median_bo)

	print(data_nl_norm)
	print(data_st_norm)
	print(data_bo_norm)

def plotGen():
	#results_dir='/home/glopez/MIRI/Q3/APA/apa_dpc2/local_results'
	xmin = 1
	xmax = len(benchmarks)
	xstep = 1
	#xticks = np.arange(xmin,xmax+xstep,xstep)
	xticks = np.arange(len(benchmarks)+1)
	width = 1.0 / 8.0

	color = ["blue", "red", "green", "black", "orange"]

	#print(xticks)
	#x = np.arange(len(benchmarks))
	#print(x)

	fig, ax = plt.subplots()
 	#fig.set_size_inches(24.0,12.0)
	fig.set_size_inches(12.0,6.0)
	#fig.set_size_inches(6.0,3.0)

	bar_color = 0.7
	bar_position = 2
	c=0
	for p in prefetchers:
		values = []
		bar_color = bar_color - 0.05
		label = '{}'.format(p)
		bar_position = bar_position - 1.0
		for b in benchmarks:
			if (p == "st_exa"):
				aux = 1
			else:
				aux = get_ipc(results_dir,b,p)
			values.append(0 if aux == None else aux)
		
		if (p != "st_exa"):
			m = gmean(values)/gmean(data_st)
			for i in range (0,len(benchmarks)):
				values[i]=values[i]/data_st[i]
		else:
			m = 1 #median(data_st)
		values.append(m)
		print(m)
		#print(values)
		#ax.bar(xticks-bar_position*width, values, width, color='{}'.format(color[c]), label=label)
		ax.bar(xticks-bar_position*width, values, width, color='{}'.format(bar_color), label=label)
		c = c+1
			#data_st.append(0 if get_ipc(results_dir,b,prefetchers[1]) == None else get_ipc(results_dir,b,prefetchers[1]))
			#data_bo.append(0 if get_ipc(results_dir,b,prefetchers[2]) == None else get_ipc(results_dir,b,prefetchers[2]))

	ax.set_xticks(xticks)
	ax.set_ylim([0,1.50])
	ha = ['right', 'center', 'left']
	ax.set_xticklabels(benchmarks+["Average"], rotation=35, fontsize=5, ha=ha[0])
	ax.set_ylabel('Speedup',rotation='vertical')
	ax.set_title('Speedup against stream')
	plt.legend(loc="upper left",ncol=2)
	#plt.show()
	version="speedup"
	plt.savefig('plots/performance-prefetchers-{}.pdf'.format(version), dpi=400, bbox_inches='tight', pad_inches=0.01)


def plotPrefetch():
	#results_dir='/home/glopez/MIRI/Q3/APA/apa_dpc2/local_results'
	xmin = 1
	xmax = len(benchmarks)
	xstep = 1
	#xticks = np.arange(xmin,xmax+xstep,xstep)
	xticks = np.arange(len(benchmarks)+1)
	width = 1.0 / 8.0

	color = ["blue", "red", "green", "black", "orange"]

	#print(xticks)
	#x = np.arange(len(benchmarks))
	#print(x)

	fig, ax = plt.subplots()
 	#fig.set_size_inches(24.0,12.0)
	fig.set_size_inches(12.0,6.0)
	#fig.set_size_inches(6.0,3.0)

	bar_color = 0.7
	bar_position = 2
	c=0
	for p in prefetchers:
		values = []
		bar_color = bar_color - 0.05
		label = '{}'.format(p)
		bar_position = bar_position - 1.0
		d = 0
		for b in benchmarks:
			if (p == "st_exa"):
				aux = get_total_prefetch(results_dir,b,p)
			elif p == "nl_exa":
				aux = prefetch_nl[d]
			else:
				aux = get_total_prefetch(results_dir,b,p)
			values.append(0 if aux == None else aux)
			d+=1
		
		#if (p != "st_exa"):
		m = statistics.mean(values)#/gmean(data_st)
			#for i in range (0,len(benchmarks)):
			#	values[i]=values[i]/prefetch_st[i]
		#else:
		#	m = 1 #median(data_st)
		values.append(m)
		print(m)
		#print(values)
		#ax.bar(xticks-bar_position*width, values, width, color='{}'.format(color[c]), label=label)
		ax.bar(xticks-bar_position*width, values, width, color='{}'.format(bar_color), label=label)
		c = c+1
			#data_st.append(0 if get_ipc(results_dir,b,prefetchers[1]) == None else get_ipc(results_dir,b,prefetchers[1]))
			#data_bo.append(0 if get_ipc(results_dir,b,prefetchers[2]) == None else get_ipc(results_dir,b,prefetchers[2]))

	ax.set_xticks(xticks)
	ax.set_ylim([0,1000000])
	ha = ['right', 'center', 'left']
	ax.set_xticklabels(benchmarks+["Average"], rotation=35, fontsize=5, ha=ha[0])
	ax.set_ylabel('Speedup',rotation='vertical')
	ax.set_title('Total prefetch against stream')
	plt.legend(loc="upper left",ncol=2)
	#plt.show()
	version="total"
	plt.savefig('plots/performance-prefetchers-{}.pdf'.format(version), dpi=400, bbox_inches='tight', pad_inches=0.01)
	"""print(data_nl)
	print(data_st)
	print(data_bo)
	median_nl = median(data_nl)
	median_st = median(data_st)
	median_bo = median(data_bo)

	#data_nl/median_st
	#data_st/median_st
	#data_bo/median_st
	#Normalize
	data_st_norm=[]
	data_nl_norm=[]
	data_bo_norm=[]

	for i in range(0,len(data_st)):
		#print(i)
		#print(data_st[i])
		#print(data_nl[i])
		#print(data_bo[i])
		#print(float(data_st[i]/data_st[i]))
		#print(float(data_nl[i]/data_st[i]))
		#print(float(data_bo[i]/data_st[i]))
		data_st_norm.append(float(data_st[i]/data_st[i]))
		data_nl_norm.append(float(data_nl[i]/data_st[i]))
		data_bo_norm.append(float(data_bo[i]/data_st[i]))

	N = len(benchmarks)+1
	#print(N)
	x = np.arange(N)
	xmin = 1
	#x = np.arange(xmin,N,2)
	print(x)

	data_nl_norm.append(median_nl)
	data_st_norm.append(median_st)
	data_bo_norm.append(median_bo)

	print(data_nl_norm)
	print(data_st_norm)
	print(data_bo_norm)


#data_nl =[10, 5, 30]
#data_nl.append(median(data_nl))
#data_st =[5,5,5]
#data_st.append(median(data_st))
#data_bo =[10,20,0]
#data_bo.append(median(data_bo))
#data_avg=[median(data_nl),median(data_st),median(data_bo)]

ax = plt.subplot(111)
plotGen()
#ax.bar(x-0.2, data_nl_norm,width=0.2,color='blue',align='center',label='Next Line')
#ax.bar(x    , data_st_norm,width=0.2,color='red',align='center', label='Stream')
#ax.bar(x+0.2, data_bo_norm,width=0.2,color='green',align='center',label='Best offset')
ax.set_ylabel('IPC Performance')
ax.set_xlabel('Benchmarks')
#ax.set_xlim([xmin,xmax])
ax.set_ylim([0,1.5])
ax.legend(loc='best')
ax.set_xticks(x)
ha = ['right', 'center', 'left']
ax.set_xticklabels(benchmarks+["Average"], rotation=35, fontsize=5, ha=ha[0])
version=0
plt.savefig('plots/performance-prefetchers-{}.pdf'.format(version), dpi=400, bbox_inches='tight', pad_inches=0.01)
#plt.show()"""
"""for p in prefetchers:
	values = []
	for b in benchmarks:
		results_dir='/home/glopez/MIRI/Q3/APA/apa_dpc2/local_results'
		values.append(get_total_prefetch(results_dir,b,p))
	print(values)
	#print(len(values))"""


def plotIPCAll():
	#results_dir='/home/glopez/MIRI/Q3/APA/apa_dpc2/local_results'
	xmin = 1
	xmax = len(benchmarks)
	xstep = 1
	#xticks = np.arange(xmin,xmax+xstep,xstep)
	xticks = np.arange(len(prefetchers_all))
	width = 1.0 / 8.0

	color = ["blue", "red", "green", "black", "orange"]

	#print(xticks)
	#x = np.arange(len(benchmarks))
	#print(x)

	fig, ax = plt.subplots()
 	#fig.set_size_inches(24.0,12.0)
	fig.set_size_inches(12.0,6.0)
	#fig.set_size_inches(6.0,3.0)

	bar_color = 0.5
	bar_position = 2
	c=0
	auxv = []
	for p in prefetchers_all:
		values = []
		#bar_color = bar_color - 0.05
		label = '{}'.format(p)
		bar_position = bar_position - 1.0
		for b in benchmarks:
			if (p == "st_exa"):
				aux = 1
			else:
				aux = get_ipc(results_dir,b,p)
			values.append(0 if aux == None else aux)
		
		if (p != "st_exa"):
			m = gmean(values)/gmean(data_st)
			#for i in range (0,len(benchmarks)):
				#values[i]=values[i]/data_st[i]
		else:
			m = 1 #median(data_st)
		auxv.append(m)
		print(str(m)+" "+str(p))
		c = c+1
		#ax.bar(xticks-bar_position*width, values, width, color='{}'.format(color[c]), label=label)
	ax.bar(xticks, auxv, width, color='{}'.format(bar_color), label=label)
			#data_st.append(0 if get_ipc(results_dir,b,prefetchers[1]) == None else get_ipc(results_dir,b,prefetchers[1]))
			#data_bo.append(0 if get_ipc(results_dir,b,prefetchers[2]) == None else get_ipc(results_dir,b,prefetchers[2]))

	ax.set_xticks(xticks)
	ax.set_ylim([0.90,1.5])
	ha = ['right', 'center', 'left']
	ax.set_xticklabels(prefetchers_all, rotation=35, fontsize=5, ha=ha[0])
	ax.set_ylabel('Speedup',rotation='vertical')
	ax.set_title('Speedup against stream')
	plt.legend(loc="upper left",ncol=2)
	#plt.show()
	version="speedup"
	plt.savefig('plots/performance-prefetchers-ipc-history-{}.pdf'.format(version), dpi=400, bbox_inches='tight', pad_inches=0.01)

plotIPCAll()
plotGen()
plotPrefetch()




