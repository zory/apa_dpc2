#!/bin/bash

mkdir -p results

for file in traces/*xz
do
	file_out="${file##*/}"
	xzcat $file | ./nl_exa -warmup_instructions 0 -simulation_instructions 5000000 | grep 'Simulation complete.' > results/${file_out}.nl_exa &
	xzcat $file | ./st_exa -warmup_instructions 0 -simulation_instructions 5000000 | grep 'Simulation complete.' > results/${file_out}.st_exa &
	xzcat $file | ./bo_pre -warmup_instructions 0 -simulation_instructions 5000000 | grep 'Simulation complete.' > results/${file_out}.bo_pre &
done
wait

