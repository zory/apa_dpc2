#!/bin/bash
#SBATCH --qos=bsc_cs
#SBATCH --time=01:00:00
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1

#xzcat traces/$1 | ./$2 -warmup_instructions 0 -simulation_instructions 5000000 | grep 'Simulation complete.' > results/${1}.${2}
xzcat traces/$1 | ./$2 -warmup_instructions 0 -simulation_instructions 5000000 > results/${1}.${2}

