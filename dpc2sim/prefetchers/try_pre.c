//
// Data Prefetching Championship Simulator 2
// Seth Pugsley, seth.h.pugsley@intel.com
//

/*
  
  This file describes a simple next-line prefetcher.  For each input address addr,
  the next cache line is prefetched, to be filled into the L2.

 */

#include <stdio.h>
#include "../inc/prefetcher.h"

#define N_ENTRIES 4096
#define ENTRY_SIZE 4

struct S_ENTRY {
	char valid[ENTRY_SIZE];
	unsigned long long int stride[ENTRY_SIZE];
	unsigned long long int hit_addr[ENTRY_SIZE];
	unsigned long long int hits[ENTRY_SIZE];
	unsigned long long int last_addr;
};

struct S_ENTRY strider[N_ENTRIES];

// For stats
unsigned long hit_not_max = 0;

void l2_prefetcher_initialize(int cpu_num)
{
  printf("Stride Prefetcher\n");
  // you can inspect these knob values from your code to see which configuration you're runnig in
  printf("Knobs visible from prefetcher: %d %d %d\n", knob_scramble_loads, knob_small_llc, knob_low_bandwidth);
  for (int way = 0; way < N_ENTRIES; way++) {
	strider[way].last_addr = 0;
	for (int i = 0; i < ENTRY_SIZE; i++) {
		strider[way].valid[i] = 0;
		strider[way].stride[i] = 0;
		strider[way].hit_addr[i] = 0;
		strider[way].hits[i] = 0;
	}
  }
}

void l2_prefetcher_operate(int cpu_num, unsigned long long int addr, unsigned long long int ip, int cache_hit)
{
  // uncomment this line to see all the information available to make prefetch decisions
  //printf("(0x%llx 0x%llx %d %d %d) ", addr, ip, cache_hit, get_l2_read_queue_occupancy(0), get_l2_mshr_occupancy(0));

  // next line prefetcher
  // since addr is a byte address, we >>6 to get the cache line address, +1, and then <<6 it back to a byte address
  // l2_prefetch_line is expecting byte addresses
  //l2_prefetch_line(0, addr, ((addr>>6)+1)<<6, FILL_L2);

  unsigned long long int way = ip % N_ENTRIES;
  long stride = addr - strider[way].last_addr;
  strider[way].last_addr = addr;

  for (int i = 0; i < ENTRY_SIZE; i++) {
	if (strider[way].valid[i]) {
		if (strider[way].stride[i] == stride) {
			strider[way].hit_addr[i] = addr;
			strider[way].hits[i]++;
			break;
		}
	} else {
		strider[way].valid[i] = 1;
		strider[way].stride[i] = stride;
		strider[way].hit_addr[i] = addr;
		strider[way].hits[i] = 1;
		break;
	}
  }

  unsigned long long int pf_addr;
  char found = 0;
  for (int i = 0; i < ENTRY_SIZE; i++) {
	if (strider[way].valid[i] && strider[way].hit_addr[i]>>6 == addr>>6) {
		hit_not_max++;
		pf_addr = addr + strider[way].stride[i];
		found = 1;
		break;
	}
  }

  if (!found) {
	  unsigned long long int max_hits = 0;
	  for (int i = 0; i < ENTRY_SIZE; i++) {
		if (strider[way].valid[i] && strider[way].hits[i] > max_hits) {
			max_hits = strider[way].hits[i];
			pf_addr = addr + strider[way].stride[i];
			found = 1;
		}
	}
  }

  if (found && addr>>6 != pf_addr>>6)
	l2_prefetch_line(0, addr, pf_addr, FILL_L2);
}

void l2_cache_fill(int cpu_num, unsigned long long int addr, int set, int way, int prefetch, unsigned long long int evicted_addr)
{
  // uncomment this line to see the information available to you when there is a cache fill event
  //printf("0x%llx %d %d %d 0x%llx\n", addr, set, way, prefetch, evicted_addr);
}

void l2_prefetcher_heartbeat_stats(int cpu_num)
{
  unsigned long long int occu = 0;
  unsigned long long int not_occu = 0;
  for (int way = 0; way < N_ENTRIES; way++) {
	for (int i = 0; i < ENTRY_SIZE; i++) {
		if (strider[way].valid[i]) occu++;
		else not_occu++;
	}
  }
  printf("Occu %llu not %llu hits_not_max %lu\n",occu,not_occu,hit_not_max);
}

void l2_prefetcher_warmup_stats(int cpu_num)
{
  printf("Prefetcher warmup complete stats\n\n");
}

void l2_prefetcher_final_stats(int cpu_num)
{
  printf("Prefetcher final stats\n");
}
