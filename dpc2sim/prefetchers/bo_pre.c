#include <stdio.h>
#include "../inc/prefetcher.h"

#define SIZE 256 // TO CHANGE 128,64
int table_head = 0;
unsigned long long int table_addr[SIZE];
unsigned long long int table_time[SIZE];
unsigned long long int stat_total_prefetches = 0;
unsigned long long int stat_total_prefetches_else = 0;
int table_off[SIZE];

int best_offset = 1;

//#define OFF_SIZE 27
#define OFF_SIZE 52
#define SCORE_MAX 31
#define ROUND_MAX 200
#define MSHR_THRESHOLD_GOOD 8
#define MSHR_THRESHOLD_BW 2
int BAD_SCORE = 1;
//int ROM_offsets[OFF_SIZE] = {-1,-2,-3,-4,-5,-6,-8,-16,-18,-20,1,2,3,4,5,6,8,16,18,20,24,25,27,30,32,36,40};

int ROM_offsets[OFF_SIZE] = {1,2,3,4,5,6,8,9,10,12,15,16,18,20,24,25,27,30,32,36,40,45,48,50,54,60,
							-1,-2,-3,-4,-5,-6,-8,-9,-10,-12,-15,-16,-18,-20,-24,-25,-27,-30,-32,-36,-40,-45,-48,-50,-54,-60};

//int ROM_offsets[OFF_SIZE] = {-1,-2,-3,-4,-5,-6,-8,-9,-10,-12,-15,-16,-18,-20,-24,-25,-27,-30,-32,-36,-40,-45,-48,-50,-54,-60,
//								1,2,3,4,5,6,8,9,10,12,15,16,18,20,24,25,27,30,32,36,40,45,48,50,54,60
//							};


int score_offsets[OFF_SIZE] = {0};
unsigned long long int score_offsets_total[OFF_SIZE] = {0}; // stats
int off_index = 0;
int round_index = 0;
int best_score = 31;

#define GAUGE_MAX 32
#define LOW_SCORE 20
int RATE_MAX = 40;
int BW = 16;
int MSHR_threshold = 12;
int gauge = 0;
int rate = 0;
unsigned long long int last_llc_time = 0;
	
void l2_prefetcher_initialize(int cpu_num)
{
  printf("Best-Offset Prefetcher\n");
  // you can inspect these knob values from your code to see which configuration you're runnig in
  printf("Knobs visible from prefetcher: %d %d %d\n", knob_scramble_loads, knob_small_llc, knob_low_bandwidth);
  if (knob_small_llc) {
	BAD_SCORE = 10;
  }
  if (knob_low_bandwidth) {
	RATE_MAX = 140;
	BW = 64;
  }
}

void l2_prefetcher_operate(int cpu_num, unsigned long long int addr, unsigned long long int ip, int cache_hit)
{
	unsigned long long int now = get_current_cycle(0);
	int mshr_occu = get_l2_mshr_occupancy(0);
	// uncomment this line to see all the information available to make prefetch decisions
	//printf("(0x%llx 0x%llx %d %d %d) ", addr, ip, cache_hit, get_l2_read_queue_occupancy(0), get_l2_mshr_occupancy(0));

	// Update MSHR threshold
	/*if (best_score > LOW_SCORE || rate > 2*BW) {
		MSHR_threshold = 12;
	} else if (rate < BW) {
		MSHR_threshold = 2;
	} else {
		MSHR_threshold = 2 + 10 * (rate-BW) / BW;
	}*/

	// Update MSHR threshold
	if (best_score > LOW_SCORE || rate > 2*BW) {
		MSHR_threshold = MSHR_THRESHOLD_GOOD;//8
	} else if (rate < BW) {
		MSHR_threshold = MSHR_THRESHOLD_BW;//2;
	} else {
		MSHR_threshold = MSHR_THRESHOLD_BW + (MSHR_THRESHOLD_GOOD-2) * (rate-BW) / BW;
	}

	// Prefetch with best offset
	if (best_score > BAD_SCORE && mshr_occu < MSHR_threshold) {
		int status = l2_prefetch_line(0, addr, ((addr>>6) + best_offset) << 6, FILL_L2);
		if (status) {
			for (int i = 0; i < OFF_SIZE; i++) {
				if (ROM_offsets[i] == best_offset) score_offsets_total[i]++; // stats
			}
			stat_total_prefetches++;
		}		
	}
	/*else if (best_score > BAD_SCORE && mshr_occu < MSHR_threshold+2) {
		int status = l2_prefetch_line(0, addr, ((addr>>6) + best_offset) << 6, FILL_LLC);
		if (status) {
			for (int i = 0; i < OFF_SIZE; i++) {
				if (ROM_offsets[i] == best_offset) score_offsets_total[i]++; // stats
			}
			stat_total_prefetches_else++;
		}
	} */

	// Update score
	unsigned long long int addr_check = addr >> 6;
	for (int i = 0; i < SIZE; i++) {
		if (addr_check == table_addr[i]) {
			score_offsets[table_off[i]]++;
			if (now > table_time[i]) {
				score_offsets[table_off[i]]++;
			}
		}
	}

	// Fill table
	table_addr[table_head] = (addr>>6) + ROM_offsets[off_index];
	table_off[table_head] = off_index;
	table_time[table_head] = now + 60;
	table_head++;
	if (table_head >= SIZE) table_head = 0;

	// Update offset indexes
	off_index++;
	if (off_index >= OFF_SIZE) {
		off_index = 0;
		round_index++;
	}

	// Check score max and finish
	int aux_best_score = 0;
	int aux_best_offset = 1;
	for (int i = 0; i < OFF_SIZE; i++) {
		if (score_offsets[i] >= aux_best_score) {
			aux_best_score = score_offsets[i];
			aux_best_offset = ROM_offsets[i];
		}
	}
	if (round_index > ROUND_MAX || aux_best_score > SCORE_MAX) {
		best_offset = aux_best_offset;
		best_score = aux_best_score;
		off_index = 0;
		round_index = 0;
		for (int i = 0; i < OFF_SIZE; i++) score_offsets[i] = 0;
	}
	if (!cache_hit) {
		gauge += (now - last_llc_time) - rate;
		last_llc_time = now;
		if (gauge > GAUGE_MAX) {
			if (rate < RATE_MAX) {
				rate++;
				gauge = 0;
			} else {
				gauge = GAUGE_MAX;
			}
		} else if (gauge < 0) {
			if (rate > 0) {
				rate--;
				gauge = GAUGE_MAX;
			} else {
				gauge = 0;
			}
		}
	}
}

void l2_cache_fill(int cpu_num, unsigned long long int addr, int set, int way, int prefetch, unsigned long long int evicted_addr)
{
//	unsigned long long int now = get_current_cycle(0);
  // uncomment this line to see the information available to you when there is a cache fill event
  //printf("0x%llx %d %d %d 0x%llx\n", addr, set, way, prefetch, evicted_addr);
}

void l2_prefetcher_heartbeat_stats(int cpu_num)
{
  printf("BOff: %d BScore: %d Rate: %d\n", best_offset, best_score, rate);
}

void l2_prefetcher_warmup_stats(int cpu_num)
{
  printf("Prefetcher warmup complete stats\n\n");
}

void l2_prefetcher_final_stats(int cpu_num)
{
 	printf("Prefetcher final stats\n");
	for (int i = 0; i < OFF_SIZE; i++) {
		printf("i%d %llu\n",ROM_offsets[i],score_offsets_total[i]);
	}
	printf("Simulation complete. Scores: ");
	for (int i = 0; i < OFF_SIZE; i++) {
		printf("%llu, ",score_offsets_total[i]);
	}
	printf("\n");

	printf("Simulation complete. Stats: total prefetch: %llu \n", stat_total_prefetches);
	printf("Simulation complete. Stats: total prefetch else: %llu \n", stat_total_prefetches_else);

}
